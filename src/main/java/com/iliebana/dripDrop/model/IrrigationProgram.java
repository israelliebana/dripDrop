package com.iliebana.dripDrop.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="irrigation_program")
public class IrrigationProgram {
    @Id
    @Column(name="irrigation_program_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="start_time")
    private Date startTime;

    @Column(name="duration")
    private Long durationInMins;

    @Column(name="week_pattern")
    private String weekPattern;

    @ManyToOne
    @JoinColumn(name="irrigation_zone_id")
    private IrrigationZone irrigationZone;

    public Long getId() {
        return id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Long getDurationInMins() {
        return durationInMins;
    }

    public String getWeekPattern() {
        return weekPattern;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setDurationInMins(Long durationInMins) {
        this.durationInMins = durationInMins;
    }

    public void setWeekPattern(String weekPattern) {
        this.weekPattern = weekPattern;
    }

    public void setIrrigationZone(IrrigationZone irrigationZone) {
        this.irrigationZone = irrigationZone;
    }
}
