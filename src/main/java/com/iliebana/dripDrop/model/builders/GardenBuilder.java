package com.iliebana.dripDrop.model.builders;

import com.iliebana.dripDrop.model.Garden;
import com.iliebana.dripDrop.model.IrrigationProgram;
import com.iliebana.dripDrop.model.IrrigationZone;

import java.util.ArrayList;
import java.util.List;

public class GardenBuilder {

    public static Garden build(GardenData gardenData){
            Garden result= new Garden();
            result.setName(gardenData.getName());
            result.setPhoneNumber(gardenData.getPhoneNumber());

            List<IrrigationZone> zones= new ArrayList<IrrigationZone>();
            result.setIrrigationZones(zones);

            if(gardenData.getIrrigationZones()==null){
                return result;
            }

            for(IrrigationZoneData zd: gardenData.getIrrigationZones()){
                IrrigationZone zone= new IrrigationZone();
                zone.setName(zd.getName());
                zone.setGarden(result);
                zone.setElectrovalveOutput(zd.getElectrovalveOutput());
                zones.add(zone);

                List<IrrigationProgram> programs= new ArrayList<IrrigationProgram>();
                zone.setIrrigationPrograms(programs);

                if(zd.getIrrigationPrograms()==null){
                    break;
                }

                for(IrrigationProgramData id: zd.getIrrigationPrograms()) {
                    IrrigationProgram program= new IrrigationProgram();
                    program.setIrrigationZone(zone);
                    program.setDurationInMins(id.getDurationInMins());
                    program.setWeekPattern(id.getWeekPattern());
                    program.setStartTime(id.getStartTime());

                    programs.add(program);
                }
            }

            return result;
    }



}
