package com.iliebana.dripDrop.model.builders;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class GardenData {
    @NotNull
    private String name;

    @NotEmpty
    @Size(min = 1)
    private List<IrrigationZoneData> irrigationZones;

    @NotNull
    private String phoneNumber;

    public GardenData() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<IrrigationZoneData> getIrrigationZones() {
        return irrigationZones;
    }

    public void setIrrigationZones(List<IrrigationZoneData> irrigationZones) {
        this.irrigationZones = irrigationZones;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
