package com.iliebana.dripDrop.model.builders;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

public class IrrigationZoneData {
    @NotNull
    private  String name;

    @NotEmpty
    @Size(min = 1)
    private  List<IrrigationProgramData> irrigationPrograms;

    @NotNull
    @Positive
    private Integer electrovalveOutput;

    public IrrigationZoneData() {
    }

    public String getName() {
        return name;
    }

    public List<IrrigationProgramData> getIrrigationPrograms() {
        return irrigationPrograms;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIrrigationPrograms(List<IrrigationProgramData> irrigationProgramDataList) {
        this.irrigationPrograms = irrigationProgramDataList;
    }

    public Integer getElectrovalveOutput() {
        return electrovalveOutput;
    }

    public void setElectrovalveOutput(Integer electrovalveOutput) {
        this.electrovalveOutput = electrovalveOutput;
    }
}