package com.iliebana.dripDrop.model.builders;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class IrrigationProgramData {
    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="HH:mm")
    private Date startTime;

    @Min(value=1)
    private Long durationInMins;

    @NotNull
    @NotBlank
    //TODO: validar contra un patrón
    private String weekPattern;

    public IrrigationProgramData() {
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Long getDurationInMins() {
        return durationInMins;
    }

    public void setDurationInMins(Long durationInMins) {
        this.durationInMins = durationInMins;
    }

    public String getWeekPattern() {
        return weekPattern;
    }

    public void setWeekPattern(String weekPattern) {
        this.weekPattern = weekPattern;
    }

    @Override
    public String toString() {
        return "IrrigationProgramData{" +
                "startTime=" + startTime +
                ", durationInMins=" + durationInMins +
                ", weekPattern='" + weekPattern + '\'' +
                '}';
    }
}
