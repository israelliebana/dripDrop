package com.iliebana.dripDrop.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="irrigation_zone")
@NamedNativeQuery(name = "IrrigationZone.deleteByGardenId", query ="delete from irrigation_zone where garden_id= ?1"
)
public class IrrigationZone {
    @Id
    @Column(name = "irrigation_zone_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name")
    private String name;

    @ManyToOne
    @JoinColumn(name="garden_id", nullable = false)
    private Garden garden;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "irrigationZone")
    private List<IrrigationProgram> irrigationPrograms;

    @Column(name="electrovalve_output")
    private Integer electrovalveOutput;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setGarden(Garden garden) {
        this.garden = garden;
    }

    public List<IrrigationProgram> getIrrigationPrograms() {
        return irrigationPrograms;
    }

    public void setIrrigationPrograms(List<IrrigationProgram> irrigationPrograms) {
        this.irrigationPrograms = irrigationPrograms;
    }

    @Override
    public String toString() {
        return "IrrigationZone{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", garden=" + garden +
                ", irrigationPrograms=" + irrigationPrograms +
                ", electrovalveOutput=" + electrovalveOutput +
                '}';
    }

    public Integer getElectrovalveOutput() {
        return electrovalveOutput;
    }

    public void setElectrovalveOutput(Integer electrovalveOutput) {
        this.electrovalveOutput = electrovalveOutput;
    }

    public void setName(String name) {
        this.name= name;
    }

    public String getName() {
        return name;
    }
}
