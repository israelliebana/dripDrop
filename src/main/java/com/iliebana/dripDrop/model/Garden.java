package com.iliebana.dripDrop.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "garden")
public class Garden {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="garden_id")
    private Long id;

    @Column(name="name")
    private String name;

    @OneToMany( fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "garden")
    private List<IrrigationZone> irrigationZones;

    @Column(name = "phone_number")
    private String phoneNumber;

    public Garden() {
    }

    public Long getId() {
        return id;
    }

    public List<IrrigationZone> getIrrigationZones() {
        return irrigationZones;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIrrigationZones(List<IrrigationZone> irrigationZones) {
        this.irrigationZones = irrigationZones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Garden{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", irrigationZones=" + irrigationZones +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
