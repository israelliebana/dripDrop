package com.iliebana.dripDrop.services.garden.impl;

import com.iliebana.dripDrop.model.Garden;
import com.iliebana.dripDrop.model.builders.GardenBuilder;
import com.iliebana.dripDrop.model.builders.GardenData;
import com.iliebana.dripDrop.repositories.GardenRepository;
import com.iliebana.dripDrop.repositories.IrrigationZoneRepository;
import com.iliebana.dripDrop.services.garden.GardenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GardenServiceImpl implements GardenService {
    private static Logger logger= LoggerFactory.getLogger(GardenServiceImpl.class);

    @Autowired
    private GardenRepository gardenRepository;

    @Autowired
    private IrrigationZoneRepository irrigationZoneRepository;

    @Override
    public Garden get(Long id) {
        Optional<Garden> recovered = gardenRepository.findById(id);
        //TODO: estudiar el uso de Optional
        if(recovered.isPresent()){
            return recovered.get();
        }else{
            return null;
        }
    }

    @Override
    public Garden create(GardenData gardenData) {
        Garden garden = GardenBuilder.build(gardenData);
        Garden saved = gardenRepository.saveAndFlush(garden);
        return saved;
    }

    @Override
    public Garden update(Long id, GardenData gardenData) {
        Garden newGarden = GardenBuilder.build(gardenData);

        Optional<Garden> existingGarden = gardenRepository.findById(id);
        if(existingGarden.isPresent()){
            newGarden.setId(existingGarden.get().getId());
            //borra las zonas y programaciones en cascada
            irrigationZoneRepository.deleteByGardenId(existingGarden.get().getId());
            // actualiza el garden y sus zonas y programaciones en cascada
            Garden saved = gardenRepository.saveAndFlush(newGarden);
            return saved;
        }else{
            return null;
        }
    }

    @Override
    public boolean delete(Long gardenId) {
        boolean result= false;

        Optional<Garden> garden = gardenRepository.findById(gardenId);
        if(garden.isPresent()) {
            gardenRepository.deleteById(gardenId);
            result= true;
        }else{
            logger.warn(String.format("Garden: %s to delete doesn't exist"));
            result= false;
        }

        return result;
    }

    @Override
    public List<Garden> list() {
        return gardenRepository.findAll();
    }
}
