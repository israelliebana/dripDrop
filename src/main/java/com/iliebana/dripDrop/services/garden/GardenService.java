package com.iliebana.dripDrop.services.garden;

import com.iliebana.dripDrop.model.Garden;
import com.iliebana.dripDrop.model.builders.GardenData;

import java.util.List;

public interface GardenService {
    Garden get(Long id);
    Garden create(GardenData gardenData);
    Garden update(Long id, GardenData gardenData);
    boolean delete(Long gardenId);
    List<Garden> list();
}
