package com.iliebana.dripDrop.services.programscheduler.job;

import com.iliebana.dripDrop.services.gardenscheduler.IrrigationProgramScheduleData;
import com.iliebana.dripDrop.services.programscheduler.ProgramAction;
import com.iliebana.dripDrop.services.programscheduler.job.ProgramJob;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProgramJobBuilder {
    public static final String DESTINATION_PHONE_NUMBER_KEY="destinationPhoneNumber";
    public static final String ACTION_KEY ="action" ;
    public static final String ELECTROVALVE_OUTPUT_KEY ="electrovalveOutput" ;


    public static List<JobDetail> buildJobs(IrrigationProgramScheduleData programScheduleData){
        List<JobDetail> result= new ArrayList<JobDetail>();
        result.add(buildJob(programScheduleData.getDestinationPhoneNumber(), ProgramAction.START, programScheduleData.getElectrovalveOutput()));
        result.add(buildJob(programScheduleData.getDestinationPhoneNumber(), ProgramAction.STOP, programScheduleData.getElectrovalveOutput()));

        return result;
    }

    private static JobDetail buildJob(String destinationPhoneNumber, ProgramAction action, Integer electrovalveOutput) {
        JobDataMap jobDataMap= new JobDataMap();
        jobDataMap.put(DESTINATION_PHONE_NUMBER_KEY, destinationPhoneNumber);
        jobDataMap.put(ACTION_KEY, action);
        jobDataMap.put(ELECTROVALVE_OUTPUT_KEY, electrovalveOutput);

        return JobBuilder.newJob(ProgramJob.class)
                .withIdentity(UUID.randomUUID().toString(), "sms-jobs")
                .withDescription("Program "+ action.toString() +" Job")
                .usingJobData(jobDataMap)
                .build();
    }

}
