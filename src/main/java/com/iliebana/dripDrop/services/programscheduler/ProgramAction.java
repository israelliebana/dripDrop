package com.iliebana.dripDrop.services.programscheduler;

public enum ProgramAction {
     STOP(0), START(1);

    public final int value;

     private ProgramAction(int value){
         this.value= value;
     }
}
