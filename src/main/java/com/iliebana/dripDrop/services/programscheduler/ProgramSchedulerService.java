package com.iliebana.dripDrop.services.programscheduler;

import com.iliebana.dripDrop.services.gardenscheduler.IrrigationProgramScheduleData;
import com.iliebana.dripDrop.services.programscheduler.result.ProgramSchedulingResult;

public interface ProgramSchedulerService {

    ProgramSchedulingResult scheduleProgram(IrrigationProgramScheduleData programScheduleData, Long gardenId);
}
