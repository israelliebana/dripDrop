package com.iliebana.dripDrop.services.programscheduler.job;

import com.iliebana.dripDrop.services.programscheduler.ProgramAction;
import com.iliebana.dripDrop.services.sms.SmsMessage;
import com.iliebana.dripDrop.services.sms.SmsMessageBuilder;
import com.iliebana.dripDrop.services.sms.SmsService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
public class ProgramJob extends QuartzJobBean{
    private static Logger logger= LoggerFactory.getLogger(ProgramJob.class);

    @Autowired
    private SmsService smsService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        logger.info(String.format("Executing job: %s ", context.getJobDetail().getKey().getName()));

        String destinationPhoneNumber = (String) context.getJobDetail().getJobDataMap().get(ProgramJobBuilder.DESTINATION_PHONE_NUMBER_KEY);
        ProgramAction action= (ProgramAction) context.getJobDetail().getJobDataMap().get(ProgramJobBuilder.ACTION_KEY);
        Integer electrovalveOutput= (Integer) context.getJobDetail().getJobDataMap().get(ProgramJobBuilder.ELECTROVALVE_OUTPUT_KEY);
        SmsMessage message=  SmsMessageBuilder.build(destinationPhoneNumber, action, electrovalveOutput);

        smsService.sendMessage(message);
        logger.info(String.format("Sent sms message: %s", message.toString()));
    }
}
