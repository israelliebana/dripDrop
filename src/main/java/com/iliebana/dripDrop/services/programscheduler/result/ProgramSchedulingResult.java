package com.iliebana.dripDrop.services.programscheduler.result;

import java.util.Date;

public class ProgramSchedulingResult {
    private Date scheduledStartTime;
    private Date scheduledStopTime;

    public Date getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(Date scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }

    public Date getScheduledStopTime() {
        return scheduledStopTime;
    }

    public void setScheduledStopTime(Date scheduledStopTime) {
        this.scheduledStopTime = scheduledStopTime;
    }
}
