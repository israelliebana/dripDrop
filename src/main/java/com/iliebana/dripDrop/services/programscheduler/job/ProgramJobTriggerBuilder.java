package com.iliebana.dripDrop.services.programscheduler.job;

import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import java.util.Date;

public class ProgramJobTriggerBuilder {

    public static Trigger buildJobTrigger(JobDetail jobDetail, Date startTime){
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), "sms-triggers")
                .withDescription("Send Sms Trigger")
                .startAt(startTime)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }
}
