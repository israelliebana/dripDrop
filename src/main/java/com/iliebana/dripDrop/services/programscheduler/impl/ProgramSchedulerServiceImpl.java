package com.iliebana.dripDrop.services.programscheduler.impl;

import com.iliebana.dripDrop.services.gardenscheduler.IrrigationProgramScheduleData;
import com.iliebana.dripDrop.services.jobsregistry.JobsRegistryService;
import com.iliebana.dripDrop.services.programscheduler.*;
import com.iliebana.dripDrop.services.programscheduler.job.ProgramJobBuilder;
import com.iliebana.dripDrop.services.programscheduler.job.ProgramJobTriggerBuilder;
import com.iliebana.dripDrop.services.programscheduler.result.ProgramSchedulingResult;
import com.iliebana.dripDrop.utils.DateUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ProgramSchedulerServiceImpl implements ProgramSchedulerService {
    private static Logger logger= LoggerFactory.getLogger(ProgramSchedulerServiceImpl.class);

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private JobsRegistryService jobsRegistryService;

    public ProgramSchedulingResult scheduleProgram(IrrigationProgramScheduleData programScheduleData, Long gardenId) {
        ProgramSchedulingResult result= new ProgramSchedulingResult();

        List<JobDetail> jobs = ProgramJobBuilder.buildJobs(programScheduleData);
        Trigger trigger0= ProgramJobTriggerBuilder.buildJobTrigger(jobs.get(0), programScheduleData.getStartTime());
        Date stopTime= DateUtils.addMins(programScheduleData.getStartTime(), programScheduleData.getDurationInMins());
        Trigger trigger1= ProgramJobTriggerBuilder.buildJobTrigger(jobs.get(1), stopTime);

        Date scheduledTime= scheduleJob(jobs.get(0), trigger0);
        result.setScheduledStartTime(scheduledTime);
        if(scheduledTime!=null){
            jobsRegistryService.registerJob(jobs.get(0).getKey(), gardenId);
        }

        scheduledTime= scheduleJob(jobs.get(1), trigger0);
        result.setScheduledStopTime(scheduledTime);
        if(scheduledTime!=null){
            jobsRegistryService.registerJob(jobs.get(1).getKey(), gardenId);
        }

        return result;
    }

    private Date scheduleJob(JobDetail jobDetail, Trigger trigger) {
        Date result = null;

        try {
            result = scheduler.scheduleJob(jobDetail, trigger);
            logger.info(String.format("Scheduled job: %s, at: %s", jobDetail.getKey().getName(), result));
        } catch (SchedulerException e) {
            logger.error(e.toString());
        }

        return result;
    }
}
