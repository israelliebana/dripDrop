package com.iliebana.dripDrop.services.jobsregistry.impl;

import com.iliebana.dripDrop.services.jobsregistry.JobsRegistryService;
import org.quartz.JobKey;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class JobsRegistryServiceImpl implements JobsRegistryService {
    private ConcurrentHashMap<Long, List<JobKey>> jobs= new ConcurrentHashMap<Long, List<JobKey>>();

    @Override
    public void registerJob(JobKey key, Long gardenId) {
        List<JobKey> jobsOfGarden = jobs.get(gardenId);
        if(jobsOfGarden==null){
            jobsOfGarden= new ArrayList<JobKey>();
            jobs.put(gardenId, jobsOfGarden);
        }

        jobsOfGarden.add(key);
    }

    @Override
    public List<JobKey> getJobsOfGarden(Long gardenId) {
        List<JobKey> result = jobs.get(gardenId);
        if(result == null){
            result= Collections.emptyList();
        }

        return result;
    }

    @Override
    public List<JobKey> removeJobsOfGarden(Long gardenId) {
        return jobs.remove(gardenId);
    }
}
