package com.iliebana.dripDrop.services.jobsregistry;

import org.quartz.JobKey;

import java.util.List;

public interface JobsRegistryService {
    void registerJob(JobKey key, Long gardenId );
    List<JobKey> getJobsOfGarden(Long gardenId);
    List<JobKey> removeJobsOfGarden(Long gardenId);
}
