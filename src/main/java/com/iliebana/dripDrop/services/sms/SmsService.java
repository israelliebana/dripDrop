package com.iliebana.dripDrop.services.sms;

public interface SmsService {
    boolean sendMessage(SmsMessage message);
}
