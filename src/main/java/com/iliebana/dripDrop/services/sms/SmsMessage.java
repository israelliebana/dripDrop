package com.iliebana.dripDrop.services.sms;

public class SmsMessage {
    private String content;
    private String destinationPhoneNumber;

    public SmsMessage(String content, String destinationPhoneNumber) {
        this.content = content;
        this.destinationPhoneNumber = destinationPhoneNumber;
    }

    public String getContent() {
        return content;
    }

    public String getDestinationPhoneNumber() {
        return destinationPhoneNumber;
    }

    @Override
    public String toString() {
        return "Message{" +
                "content='" + content + '\'' +
                ", destinationPhoneNumber='" + destinationPhoneNumber + '\'' +
                '}';
    }
}
