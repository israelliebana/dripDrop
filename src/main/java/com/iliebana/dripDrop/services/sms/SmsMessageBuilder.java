package com.iliebana.dripDrop.services.sms;

import com.iliebana.dripDrop.services.programscheduler.ProgramAction;

public class SmsMessageBuilder {
    public static SmsMessage build(String destinationPhoneNumber, ProgramAction action, Integer electrovalveOutput){
        String content= "EV:"+electrovalveOutput + ",ACTION: " + action.value ;
        SmsMessage result= new SmsMessage(content, destinationPhoneNumber);
        return result;
    }
}
