package com.iliebana.dripDrop.services.sms.twilio;

import com.iliebana.dripDrop.services.sms.SmsMessage;
import com.iliebana.dripDrop.services.sms.SmsService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;

@Service
public class SmsServiceTwilioImpl implements SmsService {

    private static final String ACCOUNT_SID = "AC9b1253a88928bf4d61984a9228e613ea";
    private static final String AUTH_TOKEN = "3ccb167d03a206ff72b44c0e6268d3a6";
    private static final String FROM_PHONE_NUMBER = "+13462630486";

    @Override
    public boolean sendMessage(SmsMessage msg) {
        PhoneNumber to= new PhoneNumber(msg.getDestinationPhoneNumber());
        PhoneNumber from= new PhoneNumber(FROM_PHONE_NUMBER);

        Message twilioMsg = Message.creator(to, from, msg.getContent()).create();

        return twilioMsg.getSid()!=null;
    }

    @PostConstruct
    private void postConstruct(){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }
}
