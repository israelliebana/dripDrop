package com.iliebana.dripDrop.services.gardenscheduler;

import com.iliebana.dripDrop.services.programscheduler.result.GardenSchedulingResult;

public interface GardenSchedulerService {

    GardenSchedulingResult scheduleGarden(GardenScheduleData gardenScheduleData);

    boolean stopScheduleGarden(Long gardenId);
}
