package com.iliebana.dripDrop.services.gardenscheduler;

import java.util.List;

public class GardenScheduleData {
    private Long gardenId;
    private String phoneNumber;
    private List<IrrigationZoneScheduleData> irrigationZoneScheduleDataList;

    public Long getGardenId() {
        return gardenId;
    }

    public void setGardenId(Long gardenId) {
        this.gardenId = gardenId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<IrrigationZoneScheduleData> getIrrigationZoneScheduleDataList() {
        return irrigationZoneScheduleDataList;
    }

    public void setIrrigationZoneScheduleDataList(List<IrrigationZoneScheduleData> irrigationZoneScheduleDataList) {
        this.irrigationZoneScheduleDataList = irrigationZoneScheduleDataList;
    }

}
