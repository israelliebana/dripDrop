package com.iliebana.dripDrop.services.gardenscheduler;

import com.iliebana.dripDrop.model.Garden;
import com.iliebana.dripDrop.model.IrrigationProgram;
import com.iliebana.dripDrop.model.IrrigationZone;

import java.util.ArrayList;
import java.util.List;

public class GardenScheduleDataBuilder {

    public static GardenScheduleData build(Garden garden){
        GardenScheduleData result= new GardenScheduleData();
        result.setGardenId(garden.getId());

        List<IrrigationZoneScheduleData> zonesData= new ArrayList<IrrigationZoneScheduleData>();
        result.setIrrigationZoneScheduleDataList(zonesData);

        for(IrrigationZone zone: garden.getIrrigationZones()){
            IrrigationZoneScheduleData zoneData= new IrrigationZoneScheduleData();
            zonesData.add(zoneData);
            zone.setElectrovalveOutput(zone.getElectrovalveOutput());
            List<IrrigationProgramScheduleData> programsData= new ArrayList<IrrigationProgramScheduleData>();
            zoneData.setProgramScheduleDataList(programsData);

            for(IrrigationProgram program: zone.getIrrigationPrograms()) {
                IrrigationProgramScheduleData programData= new IrrigationProgramScheduleData();
                programData.setDestinationPhoneNumber(garden.getPhoneNumber());
                programData.setStartTime(program.getStartTime());
                programData.setWeekpattern(program.getWeekPattern());
                programData.setDurationInMins(program.getDurationInMins());
                programData.setElectrovalveOutput(zone.getElectrovalveOutput());
                programsData.add(programData);
            }
        }

        return result;
    }
}
