package com.iliebana.dripDrop.services.gardenscheduler.impl;

import com.iliebana.dripDrop.services.gardenscheduler.GardenScheduleData;
import com.iliebana.dripDrop.services.gardenscheduler.GardenSchedulerService;
import com.iliebana.dripDrop.services.gardenscheduler.IrrigationProgramScheduleData;
import com.iliebana.dripDrop.services.gardenscheduler.IrrigationZoneScheduleData;
import com.iliebana.dripDrop.services.jobsregistry.JobsRegistryService;
import com.iliebana.dripDrop.services.programscheduler.ProgramSchedulerService;
import com.iliebana.dripDrop.services.programscheduler.result.GardenSchedulingResult;
import com.iliebana.dripDrop.services.programscheduler.result.ProgramSchedulingResult;
import com.iliebana.dripDrop.services.programscheduler.result.ZoneSchedulingResult;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GardenSchedulerServiceImpl implements GardenSchedulerService {
    private final static Logger logger = LoggerFactory.getLogger(GardenSchedulerServiceImpl.class);

    @Autowired
    private ProgramSchedulerService programScheduler;

    @Autowired
    private JobsRegistryService jobsRegistryService;

    @Autowired
    private Scheduler scheduler;

    @Override
    public GardenSchedulingResult scheduleGarden(GardenScheduleData gardenScheduleData) {
        GardenSchedulingResult result = new GardenSchedulingResult();

        for(IrrigationZoneScheduleData irrigationZoneData: gardenScheduleData.getIrrigationZoneScheduleDataList()){
            ZoneSchedulingResult zoneSchedulingResult= new ZoneSchedulingResult();

            for(IrrigationProgramScheduleData programScheduleData: irrigationZoneData.getProgramScheduleDataList()){
                ProgramSchedulingResult programSchedulingResult = programScheduler.scheduleProgram(programScheduleData, gardenScheduleData.getGardenId());
                zoneSchedulingResult.add(programSchedulingResult);
            }

            result.add(zoneSchedulingResult);
        }

        return result;
    }

    @Override
    public boolean stopScheduleGarden(Long gardenId) {
        boolean result= false;

        List<JobKey> jobs = jobsRegistryService.getJobsOfGarden(gardenId);
        if(jobs.isEmpty()){
            logger.warn("There isn't any job associated to garden: " + gardenId);
            return false;
        }

        try {
            result= scheduler.deleteJobs(jobs);
            jobsRegistryService.removeJobsOfGarden(gardenId);
        } catch (SchedulerException e) {
           logger.error(e.toString());
        }

        return result;
    }
}
