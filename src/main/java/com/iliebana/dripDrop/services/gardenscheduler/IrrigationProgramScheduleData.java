package com.iliebana.dripDrop.services.gardenscheduler;

import java.util.Date;

public class IrrigationProgramScheduleData {
    private Date startTime;
    private Long durationInMins;
    private String weekpattern;
    private String destinationPhoneNumber;
    private Integer electrovalveOutput;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getDestinationPhoneNumber() {
        return destinationPhoneNumber;
    }

    public void setDestinationPhoneNumber(String destinationPhoneNumber) {
        this.destinationPhoneNumber = destinationPhoneNumber;
    }

    public Long getDurationInMins() {
        return durationInMins;
    }

    public void setDurationInMins(Long durationInMins) {
        this.durationInMins = durationInMins;
    }

    public String getWeekpattern() {
        return weekpattern;
    }

    public void setWeekpattern(String weekpattern) {
        this.weekpattern = weekpattern;
    }

    public Integer getElectrovalveOutput() {
        return electrovalveOutput;
    }

    public void setElectrovalveOutput(Integer electrovalveOutput) {
        this.electrovalveOutput = electrovalveOutput;
    }
}
