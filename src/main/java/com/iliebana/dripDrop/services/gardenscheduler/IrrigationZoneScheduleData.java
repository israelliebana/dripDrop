package com.iliebana.dripDrop.services.gardenscheduler;

import java.util.List;

public class IrrigationZoneScheduleData {
    private Integer electrovalveOutput;
    private List<IrrigationProgramScheduleData> programScheduleDataList;

    public Integer getElectrovalveOutput() {
        return electrovalveOutput;
    }

    public void setElectrovalveOutput(Integer electrovalveOutput) {
        this.electrovalveOutput = electrovalveOutput;
    }

    public List<IrrigationProgramScheduleData> getProgramScheduleDataList() {
        return programScheduleDataList;
    }

    public void setProgramScheduleDataList(List<IrrigationProgramScheduleData> programScheduleDataList) {
        this.programScheduleDataList = programScheduleDataList;
    }
}
