package com.iliebana.dripDrop.controllers;

import com.iliebana.dripDrop.model.Garden;
import com.iliebana.dripDrop.services.garden.GardenService;
import com.iliebana.dripDrop.services.gardenscheduler.GardenScheduleData;
import com.iliebana.dripDrop.services.gardenscheduler.GardenScheduleDataBuilder;
import com.iliebana.dripDrop.services.gardenscheduler.GardenSchedulerService;
import com.iliebana.dripDrop.services.programscheduler.result.GardenSchedulingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/gardenscheduling/")
public class GardenSchedulerController {
    private static Logger logger= LoggerFactory.getLogger(GardenSchedulerController.class);

    @Autowired
    private GardenService gardenService;

    @Autowired
    private GardenSchedulerService gardenSchedulerService;

    @RequestMapping(value = "start/{id}", method = RequestMethod.GET)
    public ResponseEntity<GardenSchedulingResult> startGardenScheduling(@PathVariable Long id){
        GardenSchedulingResult result= null;
        HttpStatus statusCode= null;

        Garden garden = gardenService.get(id);
        if(garden!=null){
            GardenScheduleData gardenScheduleData= GardenScheduleDataBuilder.build(garden);
            result = gardenSchedulerService.scheduleGarden(gardenScheduleData);
            statusCode= HttpStatus.OK;
        }else{
            logger.error(String.format("Garden: %s does not exist", id));
            statusCode= HttpStatus.BAD_REQUEST;
        }

      return new ResponseEntity<>(result, statusCode);
    }

    @RequestMapping(value = "stop/{id}", method = RequestMethod.GET)
    public ResponseEntity<Long> stopGardenScheduling(@PathVariable Long id){
        boolean stopped = gardenSchedulerService.stopScheduleGarden(id);
        if(stopped){
            return new ResponseEntity<Long>(id, HttpStatus.OK);
        }else{
            return new ResponseEntity<Long>(id, HttpStatus.BAD_REQUEST);
        }
    }

}
