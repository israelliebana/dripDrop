package com.iliebana.dripDrop.controllers.errors;

public class FieldErrorMessage {
    String fieldName;
    String errorMessage;

    public FieldErrorMessage(String fieldName, String errorMessage) {
        this.fieldName = fieldName;
        this.errorMessage = errorMessage;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
