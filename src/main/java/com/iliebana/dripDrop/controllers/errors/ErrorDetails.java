package com.iliebana.dripDrop.controllers.errors;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ErrorDetails {
    private HttpStatus status;
    private String message;
    private List<String> errorList;

    public ErrorDetails(HttpStatus status, String message, List<String> errorList) {
        this.status = status;
        this.message = message;
        this.errorList = errorList;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
