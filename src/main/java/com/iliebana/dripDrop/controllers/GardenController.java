package com.iliebana.dripDrop.controllers;

import com.iliebana.dripDrop.model.Garden;
import com.iliebana.dripDrop.model.builders.GardenData;
import com.iliebana.dripDrop.services.garden.GardenService;
import com.iliebana.dripDrop.services.gardenscheduler.GardenSchedulerService;
import com.iliebana.dripDrop.services.jobsregistry.JobsRegistryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/garden")
@Validated
@CrossOrigin()
public class GardenController {
    @Autowired
    private GardenService gardenService;

    @Autowired
    private GardenSchedulerService gardenSchedulerService;

    @Autowired
    private JobsRegistryService jobsRegistryService;

    @RequestMapping(value="{id}", method = RequestMethod.GET)
    public Garden get(@PathVariable Long id){
        return gardenService.get(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Garden create(/*@Valid*/ @RequestBody GardenData gardenData){
        return gardenService.create(gardenData);
    }

    @RequestMapping(value= "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Long> delete(@PathVariable Long id){
        boolean deleted = gardenService.delete(id);
        if(deleted) {
            //se comprueba si ya se han planificado sus jobs y se paran en ese caso
            if(!jobsRegistryService.getJobsOfGarden(id).isEmpty()) {
                deleted = gardenSchedulerService.stopScheduleGarden(id);
            }
            return new ResponseEntity<>(id, HttpStatus.OK);
        }else{
            return new ResponseEntity(id, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<List<Garden>> list(){
        return new ResponseEntity(gardenService.list(), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    //TODO: falta validar el contenido del gardenData
    public ResponseEntity<Garden> update(@PathVariable Long id, @RequestBody GardenData gardenData){
        Garden result = gardenService.update(id, gardenData);
        if(result == null){
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
        }

        //se comprueba si ya se han planificado sus jobs y se paran en ese caso
        if(!jobsRegistryService.getJobsOfGarden(id).isEmpty()) {
            boolean stopScheduling = gardenSchedulerService.stopScheduleGarden(id);
            //TODO: no se devuelve por ahora este resultado
        }

        return new ResponseEntity(result, HttpStatus.OK);
    }
}
