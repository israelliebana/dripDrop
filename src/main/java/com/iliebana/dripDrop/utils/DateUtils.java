package com.iliebana.dripDrop.utils;

//import org.apache.juli.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private static Logger logger= LoggerFactory.getLogger(DateUtils.class);
    private static final SimpleDateFormat sdf= new SimpleDateFormat("HH:mm:ss");

    public static Date createDate(String timeStr){
        Date result= null;
        try {
            result = sdf.parse(timeStr);
        } catch (ParseException e) {
            logger.error(e.toString());
        }
        return result;
    }

    public static Date addMins(Date date, long mins) {
        return new Date(date.getTime()+ mins2ms(mins));
    }

    private static long mins2ms(long mins) {
        return mins*60*1000;
    }
}

