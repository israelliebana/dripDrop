package com.iliebana.dripDrop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DripDropApplication {

	public static void main(String[] args) {
		SpringApplication.run(DripDropApplication.class, args);
	}

}
