package com.iliebana.dripDrop.repositories;

import com.iliebana.dripDrop.model.IrrigationZone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface IrrigationZoneRepository extends JpaRepository<IrrigationZone, Long> {
    @Modifying(clearAutomatically = true)
    @Transactional
    //void deleteByGardenId(@Param("gardenId") Long gardenId);
    void deleteByGardenId(Long gardenId);
}
