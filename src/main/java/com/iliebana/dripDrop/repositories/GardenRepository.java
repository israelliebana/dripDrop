package com.iliebana.dripDrop.repositories;

import com.iliebana.dripDrop.model.Garden;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GardenRepository extends JpaRepository<Garden, Long> {

    List<Garden> findByName(String name);

    List<Garden> findByNameContains(String name);
    
}
