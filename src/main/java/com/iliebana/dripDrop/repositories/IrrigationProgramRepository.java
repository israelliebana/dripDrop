package com.iliebana.dripDrop.repositories;

import com.iliebana.dripDrop.model.IrrigationProgram;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IrrigationProgramRepository extends JpaRepository<IrrigationProgram, Long> {
}
