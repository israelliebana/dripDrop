package com.iliebana.dripDrop.model;

import com.iliebana.dripDrop.model.builders.*;
import com.iliebana.dripDrop.repositories.GardenRepository;
import com.iliebana.dripDrop.repositories.IrrigationProgramRepository;
import com.iliebana.dripDrop.repositories.IrrigationZoneRepository;
import com.iliebana.dripDrop.utils.DateUtils;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

//@SpringBootTest
//@Ignore
public class GardenTest {
    /*
    @Autowired
    private GardenRepository gardenRepository;

    @Autowired
    private IrrigationZoneRepository irrigationZoneRepository;

    @Autowired
    private IrrigationProgramRepository irrigationProgramRepository;

    @Test
    public void shouldInsertEmptyGarden(){
        //given
        Garden garden= new Garden();
        garden.setName("myGarden");

        //when
        Garden inserted = gardenRepository.saveAndFlush(garden);

        //then
        assertEquals(garden.getName(),inserted.getName());
    }

    @Test
    public void shouldInsertGardenWithZones(){
        //given
        String gardenName= "gardenWithZones-"+System.currentTimeMillis();
        Garden garden = createGardenWithZones(gardenName, "huerto", "23:0:0");

        //when
        gardenRepository.saveAndFlush(garden);
        for(IrrigationZone zone: garden.getIrrigationZones()){
            irrigationZoneRepository.saveAndFlush(zone);

            for(IrrigationProgram program: zone.getIrrigationPrograms()){
                irrigationProgramRepository.saveAndFlush(program);
            }
        }

        //then
        List<Garden> gardens = gardenRepository.findByName(gardenName);
        assertTrue(gardens.size() == 1);
        assertTrue(gardens.get(0).getIrrigationZones().size() == 1);
        assertTrue(gardens.get(0).getIrrigationZones().get(0).getIrrigationPrograms().size() == 1);
    }

    private Garden createGardenWithZones(String gardenName, String zoneName, String startTime) {
        IrrigationProgramData programData= new IrrigationProgramData();
        programData.setStartTime(DateUtils.createDate(startTime));
        programData.setDurationInMins(Long.valueOf(5));
        programData.setWeekPattern("L,M,X,J,V,S,D");
        IrrigationZoneData zoneData= new IrrigationZoneData();
        zoneData.setName(zoneName);
        zoneData.setElectrovalveOutput(1);
        zoneData.setIrrigationPrograms(Collections.singletonList(programData));

        GardenData result = new GardenData();
        result.setName(gardenName);
        result.setPhoneNumber("+34655289708");
        result.setIrrigationZones(Collections.singletonList(zoneData));

        return GardenBuilder.build(result);
    }

    @Test
    public void shouldFindByName(){
        List<Garden> found= gardenRepository.findByNameContains("my");
        assertTrue(found.size()>0);
    }

    @Test
    public void shouldDelete(){
        //given
        String gardenName= "gardenWithZones-"+System.currentTimeMillis();
        Garden garden = createGardenWithZones(gardenName, "huerto", "23:0:0");
        Garden inserted = gardenRepository.saveAndFlush(garden);

        //when
        gardenRepository.deleteById(inserted.getId());

        //then
        Optional<Garden> deleted = gardenRepository.findById(inserted.getId());
        assertTrue(deleted.isEmpty());
    }

    @Test
    public void shouldUpdate(){
        //given
        String gardenName= "gardenWithZones-"+System.currentTimeMillis();
        Garden garden = createGardenWithZones(gardenName,"huerto", "23:0:0");
        garden= gardenRepository.saveAndFlush(garden);

        String updatedGardenName= "gardenWithZones-"+System.currentTimeMillis();
        Garden updatedGarden = createGardenWithZones(updatedGardenName, "huertoModif", "23:30:0");
        updatedGarden.setId(garden.getId());

        //when
        //borra las zonas y programaciones en cascada
        irrigationZoneRepository.deleteByGardenId(garden.getId());
        //actualiza el nuevo garden
        Garden updated = gardenRepository.saveAndFlush(updatedGarden);

        //then
        assertTrue(updatedGarden.getName().equals(updatedGardenName));
        assertTrue(updated.getIrrigationZones().get(0).getName().equals("huertoModif"));

    }

     */
}
