package com.iliebana.dripDrop.controllers;

import com.iliebana.dripDrop.model.Garden;
import com.iliebana.dripDrop.model.builders.GardenData;
import com.iliebana.dripDrop.services.programscheduler.result.GardenSchedulingResult;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


//@SpringBootTest
//@Ignore
public class GardenSchedulerControllerIntegrationTest {
/*
    @Autowired
    private GardenSchedulerController gardenSchedulerController;

    @Autowired
    private GardenController gardenController;

    @Test
    public void shouldStartScheduling(){
        //given
        String name= "testGarden-" + System.currentTimeMillis();
        GardenData gardenData= GardenDataMocker.build(name);
        Garden created = gardenController.create(gardenData);

        //when
        ResponseEntity<GardenSchedulingResult> result = gardenSchedulerController.startGardenScheduling(created.getId());

        //then
        assertNotNull(result);
    }

    @Test
    public void shouldntStartSchedulingNonExisting(){
        //when
        ResponseEntity<GardenSchedulingResult> result = gardenSchedulerController.startGardenScheduling(Long.valueOf(-1));

        //then
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

    @Test
    public void shouldStopSchedulingAStarted(){
        //given
        String name= "testGarden-" + System.currentTimeMillis();
        GardenData gardenData= GardenDataMocker.build(name);
        Garden created = gardenController.create(gardenData);
        ResponseEntity<GardenSchedulingResult> scheduled = gardenSchedulerController.startGardenScheduling(created.getId());

        //when
        ResponseEntity<Long> result = gardenSchedulerController.stopGardenScheduling(created.getId());
        assertEquals(created.getId(), result.getBody());
    }

    @Test
    public void shouldntStopSchedulingANonStarted(){
        //when
        ResponseEntity<Long> result = gardenSchedulerController.stopGardenScheduling(Long.valueOf(-1));
        //then
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

 */

}
