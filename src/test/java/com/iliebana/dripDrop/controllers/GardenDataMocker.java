package com.iliebana.dripDrop.controllers;

import com.iliebana.dripDrop.model.builders.GardenData;
import com.iliebana.dripDrop.model.builders.IrrigationProgramData;
import com.iliebana.dripDrop.model.builders.IrrigationZoneData;
import com.iliebana.dripDrop.utils.DateUtils;

import java.util.Collections;

public class GardenDataMocker {

        public static GardenData build(String name) {
            IrrigationProgramData programData= new IrrigationProgramData();
            programData.setStartTime(DateUtils.createDate("23:10:10"));
            programData.setDurationInMins(Long.valueOf(10));
            programData.setWeekPattern("L,X,D");
            IrrigationZoneData zoneData= new IrrigationZoneData();
            zoneData.setName("testZone");
            zoneData.setElectrovalveOutput(1);
            zoneData.setIrrigationPrograms(Collections.singletonList(programData));
            GardenData result= new GardenData();
            result.setName(name);
            result.setPhoneNumber("+34655289708");
            result.setIrrigationZones(Collections.singletonList(zoneData));
            return result;
        }

}
