package com.iliebana.dripDrop.controllers;

import com.iliebana.dripDrop.model.Garden;
import com.iliebana.dripDrop.model.builders.GardenData;
import com.iliebana.dripDrop.model.builders.IrrigationProgramData;
import com.iliebana.dripDrop.model.builders.IrrigationZoneData;
import com.iliebana.dripDrop.utils.DateUtils;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.validation.ConstraintViolationException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

//TODO: este test necesita tener arrancada una BD Postgres. Investigar como levantar una BBDD embebida.

//@SpringBootTest
//@Ignore
public class GardenControllerIntegrationTest {
/*
    @Autowired
    private GardenController gardenController;

    @Test
    public void shouldCreateGarden(){
        //given
        String name= "testGarden-" + System.currentTimeMillis();
        GardenData gardenData= GardenDataMocker.build(name);

        //when
        Garden created = gardenController.create(gardenData);

        //then
        assertNotNull(created);
    }

    //TODO: tests mas exhaustivos de validaciones del modelo deberian ir en GardenTest
    @Test()
    public void shouldNotCreateNonValidGarden(){
        //given
        GardenData gardenData = new GardenData();

        //when..then
        assertThrows(ConstraintViolationException.class, () -> gardenController.create(gardenData));
    }

    @Test
    public void shouldGetGarden(){
        //given
        String name= "testGarden-" + System.currentTimeMillis();
        GardenData gardenData= GardenDataMocker.build(name);
        Garden created = gardenController.create(gardenData);

        //when
        Garden recovered = gardenController.get(created.getId());

        //then
        assertTrue(created.getId().equals(recovered.getId()));
    }

    @Test
    public void shouldDeleteGarden(){
        //given
        String name= "testGarden-" + System.currentTimeMillis();
        GardenData gardenData= GardenDataMocker.build(name);
        Garden created = gardenController.create(gardenData);

        //when
        gardenController.delete(created.getId());

        //then
        Garden recovered = gardenController.get(created.getId());
        assertNull(recovered);
    }

    @Test
    public void shouldUpdateGarden(){
        //given
        String name= "testGarden-" + System.currentTimeMillis();
        GardenData gardenData= GardenDataMocker.build(name);
        Garden created = gardenController.create(gardenData);

        //when
        String updatedName="testUpdatedGarden-" + System.currentTimeMillis();;
        GardenData updatedGardenData = GardenDataMocker.build(updatedName);
        ResponseEntity<Garden> result = gardenController.update(created.getId(), updatedGardenData);

        //then
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertEquals(updatedName, result.getBody().getName());
    }


 */
}
